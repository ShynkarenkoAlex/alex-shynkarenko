package hw1Lesson13;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean isValid;
        do {
            System.out.print("Input phone number:");
            String phoneString = in.nextLine();
            isValid = phoneString.matches("\\W(\\d{12})|(\\d{9})");
            if (isValid){
                System.out.println("Phone number is entered correctly! :" + phoneString);
            } else {
                System.out.println("Phone number is entered incorrectly! Enter 9 or 13 characters. :" + phoneString);
                System.out.println("Please try again");
            }
        } while (!isValid);

        in.close();
    }
}
