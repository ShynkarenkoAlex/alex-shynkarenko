package hw3Lesson15;

import java.util.Scanner;

public class App {

    /*
    Create a console application. Task manager.
    User should be able to create, edit, remove tasks.
    Data should be stored in app memory.

     */

    private TaskManagerIImpl taskManagerI = new TaskManagerIImpl();

    public static void main(String[] args) {
        App app = new App();
        menu();
        Scanner scanner = new Scanner(System.in);

        while (app.manageTask(scanner)) {
            menu();
        }
    }

    private static void menu() {
        System.out.println("\nHello" +
                "\nThis is the task manager supper application" +
                "\n1. Create task" +
                "\n2. Edit task" +
                "\n3. Remove task" +
                "\n4. Show all tasks" +
                "\n5. Exit  ");
    }

    private boolean manageTask(Scanner scanner) {
        int data = Integer.parseInt(scanner.next());
        switch (data) {
            case 1:
                boolean result = taskManagerI.createTask(scanner);
                if (!result) {
                    return false;
                }
                break;
            case 2:
                taskManagerI.editTask(scanner);
                break;
            case 3:
                taskManagerI.removeTask(scanner);
                break;
            case 4:
                taskManagerI.showAllTasks();
                break;
            case 5:
                System.out.println("Closing application");
                System.exit(-1);
                break;
            default:
                System.out.println("Closing application");
                System.exit(-1);
        }
        return true;
    }
}
