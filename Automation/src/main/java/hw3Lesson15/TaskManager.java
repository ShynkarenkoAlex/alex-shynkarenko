package hw3Lesson15;

import java.util.Scanner;

public interface TaskManager {
    boolean createTask(Scanner scanner);

    boolean editTask(Scanner scanner);

    boolean removeTask(Scanner scanner);

    void showAllTasks();
}
