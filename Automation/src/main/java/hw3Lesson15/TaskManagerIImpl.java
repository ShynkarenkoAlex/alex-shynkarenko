package hw3Lesson15;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TaskManagerIImpl implements TaskManager {

    private Map<Integer, Task> database = new HashMap<>();

    @Override
    public boolean createTask(Scanner scanner) {

        try {
            System.out.println("Enter task id");
            int id = Integer.parseInt(scanner.next());

            System.out.println("Enter title");
            String title = scanner.next();

            System.out.println("Enter description");
            String description = scanner.next();

            Date date = new Date();

            Task task = new Task(title, description, date);
            database.putIfAbsent(id, task);

            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean editTask(Scanner scanner) {
        try {
            System.out.println("Enter task id");
            int id = Integer.parseInt(scanner.next());

            if (database.get(id) != null) {
                System.out.println("Enter title");
                String title = scanner.next();

                System.out.println("Enter description");
                String description = scanner.next();

                Date date = new Date();

                Task task = database.get(id);

                task.setTitle(title);
                task.setDescription(description);

                task.setDate(date);
                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean removeTask(Scanner scanner) {
        try {
            System.out.println("Enter task id");
            int id = Integer.parseInt(scanner.next());

            database.remove(id);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public void showAllTasks() {
        for (Map.Entry entry : database.entrySet()) {
            System.out.println(entry);
        }
    }
}
