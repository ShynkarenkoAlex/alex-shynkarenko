package hw2Lesson14_part2;

public class FastMode extends WashingMode {

    public FastMode(EWashingRPM rpm, EWashingTemperature temperature, EWashingTime time) {
        super(rpm, temperature, time);
    }
}
