package hw2Lesson14_part2;

public enum EWashingTime {
    min15("15"), min30("30"), min60("60"), min20("20");

    public final String label;

    private EWashingTime(String label) {
        this.label = label;
    }
}
