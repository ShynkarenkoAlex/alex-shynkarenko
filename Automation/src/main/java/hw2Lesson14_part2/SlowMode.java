package hw2Lesson14_part2;

public class SlowMode extends WashingMode {

    public SlowMode(EWashingRPM rpm, EWashingTemperature temperature, EWashingTime time) {
        super(rpm, temperature, time);
    }

}
