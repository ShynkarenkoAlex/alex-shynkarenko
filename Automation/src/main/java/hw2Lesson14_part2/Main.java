package hw2Lesson14_part2;

import hw2Lesson14.EShapes;
import hw2Lesson14.IShape;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println(" Choose a type of wash ");
        for (EWashingModes model : EWashingModes.values()) {
            System.out.println(model);
        }
        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("Enter washing mode: ");
            String inputMode = in.next();
            WashingMode mode = null;
            EWashingRPM rpm = null;
            EWashingTemperature temp = null;
            EWashingTime time = null;
            if (findWashingMode(inputMode)) {
                switch (EWashingModes.valueOf(inputMode)) {
                    case Medium:
                        rpm = readRPM();
                        temp = readTemp();
                        time = readTime();
                        mode = new MediumMode(rpm, temp, time);
                        start(mode);
                        break;
                    case Slow:
                        rpm = readRPM();
                        temp = readTemp();
                        time = readTime();
                        mode = new SlowMode(rpm, temp, time);
                        start(mode);
                        break;
                    case Fast:
                        rpm = readRPM();
                        temp = readTemp();
                        time = readTime();
                        mode = new FastMode(rpm, temp, time);
                        start(mode);
                        break;
                    case Rinse:
                        rpm = readRPM();
                        temp = readTemp();
                        time = readTime();
                        mode = new RinseMode(rpm, temp, time);
                        start(mode);
                        break;
                    case Off:
                        System.out.println("Good bay");
                        return;
                    default:
                        System.out.println("Unexpected value: " + EWashingModes.valueOf(inputMode));
                }
            } else {
                System.out.println("Wrong mode!!!Please enter try.");
            }
        }
    }

    private static boolean findWashingMode(String inputMode) {
        for (EWashingModes mode : EWashingModes.values()) {
            if (mode.name().equals(inputMode)) {
                return true;
            }
        }
        return false;
    }

    private static void start(WashingMode mode) throws InterruptedException {
        System.out.println("closed");
        System.out.println("washing processing");
        ConsoleStatusBar pb2 = new ConsoleStatusBar();
        pb2.start();
        mode.start();
        pb2.showProgress = false;
        System.out.println("finished");
    }

    // processing user rpm value
    private static EWashingRPM readRPM() {
        System.out.println("Choose rpm ");
        for (EWashingRPM value : EWashingRPM.values()) {
            System.out.println(value);
        }

        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("Enter rpm : ");
            String input = in.next();
            if (findRPM(input) != null) {
                return findRPM(input);
            } else {
                System.out.println("Wrong rpm!!!Please enter try.");
            }
        }
    }

    private static EWashingRPM findRPM(String inputRPM) {
        for (EWashingRPM mode : EWashingRPM.values()) {
            if (mode.name().equals(inputRPM)) {
                return mode;
            }
        }
        return null;
    }

    // end processing user time value

    private static EWashingTime readTime() {
        System.out.println("Choose time ");
        for (EWashingTime value : EWashingTime.values()) {
            System.out.println(value);
        }

        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("Enter time : ");
            String input = in.next();
            if (findTime(input) != null) {
                return findTime(input);
            } else {
                System.out.println("Wrong time!!!Please enter try.");
            }
        }
    }

    private static EWashingTime findTime(String input) {
        for (EWashingTime mode : EWashingTime.values()) {
            if (mode.name().equals(input)) {
                return mode;
            }
        }
        return null;
    }

    // end processing user temp value

    private static EWashingTemperature readTemp() {
        System.out.println("Choose temp ");
        for (EWashingTemperature value : EWashingTemperature.values()) {
            System.out.println(value);
        }

        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("Enter temp : ");
            String input = in.next();
            if (findTemp(input) != null) {
                return findTemp(input);
            } else {
                System.out.println("Wrong temp!!!Please enter try.");
            }
        }
    }
    private static EWashingTemperature findTemp(String input) {
        for (EWashingTemperature mode : EWashingTemperature.values()) {
            if (mode.name().equals(input)) {
                return mode;
            }
        }
        return null;
    }
}



