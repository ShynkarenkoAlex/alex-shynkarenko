package hw2Lesson14_part2;

public class MediumMode extends WashingMode {

    public MediumMode(EWashingRPM rpm, EWashingTemperature temperature, EWashingTime time) {
        super(rpm, temperature, time);
    }
}
