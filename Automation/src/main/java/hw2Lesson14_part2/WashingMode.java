package hw2Lesson14_part2;

import java.util.concurrent.TimeUnit;

public abstract class WashingMode {
    protected EWashingTime time;
    protected EWashingRPM rpm;
    protected EWashingTemperature temperature;
    protected double washingTime;

    public void start() throws InterruptedException {
        while (washingTime < Double.parseDouble(time.label)){
            TimeUnit.SECONDS.sleep(1);
            washingTime = washingTime+1;
        }

    };

    public WashingMode(EWashingRPM rpm, EWashingTemperature temperature, EWashingTime time) {
        this.rpm = rpm;
        this.temperature = temperature;
        this.time = time;
        this.washingTime = 0;
    }

    public double getWashingTime() {
        return washingTime;
    }
}
