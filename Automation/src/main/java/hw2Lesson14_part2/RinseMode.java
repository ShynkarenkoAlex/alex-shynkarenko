package hw2Lesson14_part2;

public class RinseMode extends WashingMode {

    public RinseMode(EWashingRPM rpm, EWashingTemperature temperature, EWashingTime time) {
        super(rpm, temperature, time);
    }

}
