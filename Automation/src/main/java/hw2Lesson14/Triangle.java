package hw2Lesson14;

public class Triangle implements IShape {

    private double lengthSideA;

    private double lengthSideB;

    private double lengthSideC;

    private double area;

    private double perimeter;

    public Triangle(double lengthSideA, double lengthSideB, double lengthSideC) {
        this.lengthSideA = lengthSideA;
        this.lengthSideB = lengthSideB;
        this.lengthSideC = lengthSideC;
    }

    public void calculateArea() {
        if (this.perimeter == 0) {
            this.calculatePerimeter();
        }
        double halfPerimeter = this.perimeter * 0.5;
        this.area = Math.sqrt(halfPerimeter * (halfPerimeter - this.lengthSideA) * (halfPerimeter - this.lengthSideB) * (halfPerimeter - this.lengthSideC));
    }

    public void calculatePerimeter() {
        this.perimeter = (this.lengthSideA + this.lengthSideB + this.lengthSideC);
    }

    public double getArea() {
        return this.area;
    }

    public double getPerimeter() {
        return this.perimeter;
    }
}
