package hw2Lesson14;

public class Square implements IShape {

    private double lengthSide;

    private double area;

    private double perimeter;

    public Square(double lengthSide) {
        this.lengthSide = lengthSide;
    }

    public void calculateArea() {
        this.area = Math.pow(lengthSide, 2);
    }

    public void calculatePerimeter() {
        this.perimeter = 4 * lengthSide;
    }

    public double getArea() {
        return this.area;
    }

    public double getPerimeter() {
        return this.perimeter;
    }
}
