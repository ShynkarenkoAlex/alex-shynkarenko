package hw2Lesson14;

public interface IShape {
    void calculateArea();
    void calculatePerimeter();
    double getArea();
    double getPerimeter();
}
