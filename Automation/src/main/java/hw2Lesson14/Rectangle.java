package hw2Lesson14;

public class Rectangle implements IShape {

    private double length;

    private double width;

    private double area;

    private double perimeter;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public void calculateArea() {
        this.area = length * width;
    }

    public void calculatePerimeter() {
        this.perimeter = 2 * (length + width);
    }

    public double getArea() {
        return this.area;
    }

    public double getPerimeter() {
        return this.perimeter;
    }
}
