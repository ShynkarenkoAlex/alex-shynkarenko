package hw2Lesson14;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Please enter some shape: ");
        for (EShapes shape : EShapes.values()) {
            System.out.println(shape);
        }

        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("Enter shape: ");
            String inputShape = in.next();
            if (findShape(inputShape)) {
                switch (EShapes.valueOf(inputShape)) {
                    case Circle:
                        System.out.println("Please enter radius: ");
                        Circle circle = new Circle(in.nextDouble());
                        calculate(circle);
                        printMessage(circle);
                        break;
                    case Rectangle:
                        System.out.println("Please enter length: ");
                        double length = in.nextDouble();
                        System.out.println("Please enter width: ");
                        double width = in.nextDouble();
                        Rectangle rectangle = new Rectangle(length, width);
                        calculate(rectangle);
                        printMessage(rectangle);
                        break;
                    case Square:
                        System.out.println("Please enter length side: ");
                        double side = in.nextDouble();
                        Square square = new Square(side);
                        calculate(square);
                        printMessage(square);
                        break;
                    case Triangle:
                        System.out.println("Please enter side a: ");
                        double a = in.nextDouble();
                        System.out.println("Please enter side b: ");
                        double b = in.nextDouble();
                        System.out.println("Please enter side c: ");
                        double c = in.nextDouble();
                        Triangle triangle = new Triangle(a, b, c);
                        calculate(triangle);
                        printMessage(triangle);
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + EShapes.valueOf(inputShape));
                }
                return;
            } else {
                System.out.println("Wrong shape!!!Please enter try.");
            }
        }
    }

    private static void calculate(IShape shape) {
        shape.calculateArea();
        shape.calculatePerimeter();
    }

    private static boolean findShape(String inputShape) {
        for (EShapes shape : EShapes.values()) {
            if (shape.name().equals(inputShape)) {
                return true;
            }
        }
        return false;
    }

    private static void printMessage(IShape shape) {
        System.out.println("perimeter - " + shape.getPerimeter());
        System.out.println("area - " + shape.getArea());
    }
}
