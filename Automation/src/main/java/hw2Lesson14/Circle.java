package hw2Lesson14;

public class Circle implements IShape {

    private double radius;

    private double area;

    private double perimeter;

    public Circle(double radius) {
        this.radius = radius;
    }

    public void calculateArea() {
        this.area = Math.PI * Math.pow(radius, 2);
    }

    public void calculatePerimeter() {
        this.perimeter = 2 * Math.PI * radius;
    }

    public double getArea() {
        return this.area;
    }

    public double getPerimeter() {
        return this.perimeter;
    }
}
